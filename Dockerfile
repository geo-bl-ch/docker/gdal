FROM alpine:3.10

#
# Install proj4
#
ARG PROJ_VERSION=6.1.1
RUN apk --update add sqlite libstdc++ sqlite-libs libgcc && \
    apk --update add --virtual .proj4-deps \
        make \
        gcc \
        g++ \
        file \
        sqlite-dev \
        unzip && \
    cd /tmp && \
    wget http://download.osgeo.org/proj/proj-${PROJ_VERSION}.tar.gz && \
    tar xfvz proj-${PROJ_VERSION}.tar.gz && \
    rm -f proj-${PROJ_VERSION}.tar.gz && \
    wget http://download.osgeo.org/proj/proj-datumgrid-1.8.zip && \
    unzip proj-datumgrid-1.8.zip -d proj-${PROJ_VERSION}/nad/ && \
    rm -f proj-datumgrid-1.8.zip && \
    wget http://download.osgeo.org/proj/proj-datumgrid-europe-1.1.zip && \
    unzip proj-datumgrid-europe-1.1.zip -d proj-${PROJ_VERSION}/nad/ && \
    rm -f proj-datumgrid-europe-1.1.zip && \
    wget http://download.osgeo.org/proj/proj-datumgrid-north-america-1.1.zip && \
    unzip proj-datumgrid-north-america-1.1.zip -d proj-${PROJ_VERSION}/nad/ && \
    rm -f proj-datumgrid-north-america-1.1.zip && \
    wget http://download.osgeo.org/proj/proj-datumgrid-oceania-1.0.zip && \
    unzip proj-datumgrid-oceania-1.0.zip -d proj-${PROJ_VERSION}/nad/ && \
    rm -f proj-datumgrid-oceania-1.0.zip && \
    cd proj-${PROJ_VERSION} && \
    ./configure && \
    make -j 4 && \
    make install && \
    echo "Entering root folder" && \
    cd / &&\
    echo "Cleaning dependencies tmp and manuals" && \
    apk del .proj4-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man && \
    proj

#
# Install geotiff
#
ARG GEOTIFF_VERSION=1.5.1
RUN apk --update add zlib tiff libjpeg && \
    apk --update add --virtual .geotiff-deps \
        zlib \
        make \
        gcc \
        g++ \
        file \
        zlib-dev \
        tiff-dev \
        sqlite-dev \
        jpeg-dev && \
    cd /tmp && \
    wget http://download.osgeo.org/geotiff/libgeotiff/libgeotiff-${GEOTIFF_VERSION}.tar.gz && \
    tar xfz libgeotiff-${GEOTIFF_VERSION}.tar.gz  && \
    rm -f libgeotiff-${GEOTIFF_VERSION}.tar.gz && \
    cd libgeotiff-${GEOTIFF_VERSION} && \
    ./configure --with-jpeg=yes --with-zlib=yes --with-proj=/usr/local && \
    make && \
    make install && \
    cd / && \
    apk del .geotiff-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

# Install geos
ARG GEOS_VERSION=3.7.1
RUN apk --update add --virtual .geos-deps \
        which \
        make \
        gcc \
        g++ \
        file \
        git \
        autoconf \
        automake \
        libtool && \
    cd /tmp && \
    git clone https://git.osgeo.org/gitea/geos/geos.git geos && \
    cd geos && \
    git checkout ${GEOS_VERSION} && \
    ./autogen.sh && \
    ./configure && \
    make -j 4 && \
    make install && \
    cd ~ && \
    apk del .geos-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

RUN mkdir -p /build_thirdparty/usr/lib

# Build hdf5
ARG HDF5_VERSION=1.10.5
RUN apk --update add --virtual .hdf5-deps \
        make \
        gcc \
        g++ && \
    cd /tmp && \
    wget -q https://support.hdfgroup.org/ftp/HDF5/releases/hdf5-${HDF5_VERSION%.*}/hdf5-${HDF5_VERSION}/src/hdf5-${HDF5_VERSION}.tar.gz && \
    tar xzf hdf5-${HDF5_VERSION}.tar.gz && \
    cd hdf5-${HDF5_VERSION} && \
    CFLAGS=-O2 CXXFLAGS=-O2 ./configure --prefix=/usr --disable-static --with-szlib=/usr --enable-cxx && \
    make && \
    make install && \
    cp -P /usr/lib/libhdf5*.so* /build_thirdparty/usr/lib && \
    for i in /build_thirdparty/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done && \
    cd ~ && \
    apk del .hdf5-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

# Build netCDF
ARG NETCDF_VERSION=4.6.3
RUN apk --update add --virtual .netcdf-deps \
        make \
        gcc \
        curl-dev \
        m4 \
        g++ && \
    cd /tmp && \
    wget -q https://github.com/Unidata/netcdf-c/archive/v${NETCDF_VERSION}.tar.gz && \
    tar xzf v${NETCDF_VERSION}.tar.gz && \
    cd netcdf-c-${NETCDF_VERSION} && \
    CFLAGS=-O2 ./configure --prefix=/usr --disable-static && \
    make && \
    make install && \
    cp -P /usr/lib/libnetcdf*.so* /build_thirdparty/usr/lib && \
    for i in /build_thirdparty/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done && \
    cd ~ && \
    apk del .netcdf-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

# Build hdf4
ARG HDF4_VERSION=4.2.14
RUN apk --update add zlib libjpeg jpeg && \
    apk --update add --virtual .hdf4-deps \
        make \
        gcc \
        byacc \
        flex \
        portablexdr-dev \
        zlib-dev \
        jpeg-dev \
        g++ && \
    cd /tmp && \
    mkdir hdf4 && \
    wget -q https://support.hdfgroup.org/ftp/HDF/releases/HDF${HDF4_VERSION}/src/hdf-${HDF4_VERSION}.tar.gz -O - \
        | tar xz -C hdf4 --strip-components=1 && \
    cd hdf4 && \
    LDFLAGS=-lportablexdr ./configure --prefix=/usr --enable-shared --disable-static \
        --disable-fortran --disable-netcdf && \
    make && \
    make install && \
    cp -P /usr/lib/libdf*.so* /build_thirdparty/usr/lib && \
    cp -P /usr/lib/libmfhdf*.so* /build_thirdparty/usr/lib && \
    for i in /build_thirdparty/usr/lib/*; do strip -s $i 2>/dev/null || /bin/true; done && \
    cd ~ && \
    apk del .hdf4-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

#
# Install GDAL
#
ARG GDAL_VERSION=3.0.0
RUN apk --update add tiff portablexdr curl && \
    apk --update add --virtual .gdal-deps \
        make \
        gcc \
        g++ \
        file \
        postgresql-dev \
        tiff-dev \
        curl-dev \
        portablexdr-dev \
        linux-headers && \
    cd /tmp && \
    wget http://download.osgeo.org/gdal/${GDAL_VERSION}/gdal-${GDAL_VERSION}.tar.gz && \
    tar xzf gdal-${GDAL_VERSION}.tar.gz && \
    rm -f gdal-${GDAL_VERSION}.tar.gz && \
    cd gdal-${GDAL_VERSION} && \
    ./configure \
        --with-netcdf \
        --with-hdf4 \
        --with-hdf5 \
        --with-geotiff=/usr/local \
        --with-proj=/usr/local \
        --with-pg=/usr/bin/pg_config \
        --with-geos=/usr/local/bin/geos-config && \
    make && \
    make install && \
    cd ~ && \
    apk del .gdal-deps && \
    rm -rf /tmp/* && \
    rm -rf /user/local/man

RUN apk --update add bash

RUN mkdir /data && \
    chown 1001 /data && \
    chgrp 0 /data && \
    chmod g=u /data && \
    chgrp 0 /etc/passwd && \
    chmod g=u /etc/passwd

USER 1001

RUN gdalinfo --formats

COPY --chown=1001:0 uid_entrypoint.sh /usr/local/bin/

WORKDIR /data

ENTRYPOINT ["/usr/local/bin/uid_entrypoint.sh"]

CMD ["--version"]
